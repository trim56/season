//
//  ViewController.swift
//  Season
//
//  Created by Павел Зорин on 13.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var headerImage: UIImageView!
    
    @IBOutlet weak var winterButton: SeasonButton!
    @IBOutlet weak var springButton: SeasonButton!
    @IBOutlet weak var autumnButton: SeasonButton!
    @IBOutlet weak var summerButton: SeasonButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        winterButton.delegate = self
        springButton.delegate = self
        autumnButton.delegate = self
        summerButton.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerImage.footerQuadCurve()
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { (_) in
            self.headerImage.footerQuadCurve()
        }
    }
}

extension ViewController: SeasonButtonDelegate {
    func tapView(seasonImage: String?) {
        guard let image = seasonImage else {
            return
        }
        UIView.transition(
            with: self.headerImage,
            duration: 0.5,
            options: .transitionCrossDissolve,
            animations: { self.headerImage.image = UIImage(named: image) },
            completion: nil
        )
    }
}
