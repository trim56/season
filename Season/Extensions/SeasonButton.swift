//
//  SeasonButton.swift
//  Season
//
//  Created by Павел Зорин on 13.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

import UIKit

protocol SeasonButtonDelegate: class {
    func tapView(seasonImage: String?)
}

class SeasonButton: UIView {
    
    private var animator = UIViewPropertyAnimator()
    weak var delegate: SeasonButtonDelegate?
    
    let imageList: [String] = [
        "winter-header",
        "spring-header",
        "autumn-header",
        "summer-header"
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        layer.cornerRadius = 20
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(tapViewAction))
        tap.minimumPressDuration = 0
        addGestureRecognizer(tap)
    }
    
    @objc func tapViewAction(gesture: UITapGestureRecognizer) {
        // Анимация начала нажатия
        if gesture.state == .began {
            animator = UIViewPropertyAnimator(duration: 0.2, curve: .easeOut, animations: {
                self.alpha = 0.7
                self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            })
            animator.startAnimation()
        // Анимация окончания нажатия
        } else if gesture.state == .ended {
            animator = UIViewPropertyAnimator(duration: 0.2, curve: .easeOut, animations: {
                self.alpha = 1
                self.transform = .identity
            })
            animator.startAnimation()
            // Определение имени изображения, согласно нажатого блока
            let season = imageList[self.tag] 
            self.delegate?.tapView(seasonImage: season)
        }
        
    }
}
