//
//  UIImageView.swift
//  Season
//
//  Created by Павел Зорин on 13.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

import UIKit

extension UIImageView {
    func footerQuadCurve(){
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:self.bounds.size.width, y:0))
        arrowPath.addLine(
            to: CGPoint(
                x: self.bounds.size.width,
                y: self.bounds.size.height - (self.bounds.size.height * 0.2)
            )
        )
        arrowPath.addQuadCurve(
            to: CGPoint(
                x: 0,
                y: self.bounds.size.height - (self.bounds.size.height * 0.2)
            ),
            controlPoint: CGPoint(
                x: self.bounds.size.width/2,
                y: self.bounds.size.height
            )
        )
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()

        let shapeLayer = CAShapeLayer(layer: self.layer)
        shapeLayer.path = arrowPath.cgPath
        shapeLayer.frame = self.bounds
        shapeLayer.masksToBounds = true
        self.layer.mask = shapeLayer
    }
}
